﻿module kMean.Tests

open NUnit.Framework
open kMean.KMeans
open System
open MathNet.Numerics.Distributions


[<Test>]
    let ``distance between two points``()=
        let point1 = {X=0.0;Y=0.0}
        let point2 = {X=1.0;Y=0.0}

        let expectedDistance = Math.Abs(1)
        let distance = quadraticDistance point1 point2
        Assert.AreEqual(1,distance)

[<Test>]
    let ``total distance of a point respect to a given set of points``()=
        let point = {X=0.0;Y=0.0}
        let points = [{X=2.0;Y=0.0};{X=0.0;Y=2.0}]

        let expectedDistance = 8.0 // 2^2 + 2^2

        Assert.AreEqual(expectedDistance,sumOfQuadraticDistances point points)


[<Test>]
    let ``total distance between a point and the other points``()=
        let firstPoint = {X=0.0;Y=0.0}
        let secondPoint = {X=0.0;Y=1.0}
        let points = [firstPoint; secondPoint]
        let actualDistance = sumOfQuadraticDistances firstPoint points
        let expectedDistance = 1

        Assert.AreEqual(expectedDistance,actualDistance)

[<Test>]
    let ``total distance between a point and the other points 2``()=
        let firstPoint = {X=0.0;Y=0.0}
        let secondPoint = {X=0.0;Y=1.0}
        let thirdPoint = {X=0.0;Y=3.0}

        let points = [firstPoint; secondPoint;thirdPoint]
        let actualDistance = sumOfQuadraticDistances firstPoint points 
        let expectedDistance = 10 // 1^2 + 3^2

        Assert.AreEqual(expectedDistance,actualDistance)

       

[<Test>]
        let ``centroid test``()=
            let firstPoint = {X=0.0;Y=0.0}
            let secondPoint = {X=0.0;Y=1.0}
            let thirdPoint = {X=0.0;Y=2.0}

            let points = [firstPoint; secondPoint; thirdPoint]

            let (centroid,variance) = centroidAndVariance points
            Assert.AreEqual(secondPoint,centroid)
            Assert.AreEqual(2.0,variance)

[<Test>]
    let ``partitions and iterate next centroids``()=
        let points = [{X= -3.0;Y=0.0};{X= -2.0;Y=0.0};{X= -1.0;Y=0.0};{X=1.0;Y=0.0};{X=2.0;Y=0.0};{X=3.0;Y=0.0}]
        let centroidGuesses = [{X = -3.0;Y=0.0}; {X = 3.0;Y = 0.0}]

        let expected = List.rev  [
                        {Point={X= -3.0;Y=0.0};AssignedCentroid={X= -3.0;Y=0.0}};
                        {Point={X= -2.0;Y=0.0};AssignedCentroid={X= -3.0;Y=0.0}};
                        {Point={X= -1.0;Y=0.0};AssignedCentroid={X= -3.0;Y=0.0}};
                        {Point={X=  1.0;Y=0.0};AssignedCentroid={X=  3.0;Y=0.0}};
                        {Point={X=  2.0;Y=0.0};AssignedCentroid={X=  3.0;Y=0.0}};
                        {Point={X=  3.0;Y=0.0};AssignedCentroid={X=  3.0;Y=0.0}}
                     ]

        let partitioned = tagPointsWithClosestCentroid centroidGuesses points

        Assert.AreEqual(expected,partitioned)

        let expectedCentroids = [{X= 2.0; Y=0.0}; {X = -2.0;Y=0.0}]

        let actual = iterateFindingFinalCentroids points centroidGuesses

        Assert.AreEqual(expectedCentroids,actual)





// given a list of lists of points, get iteratively the centroids of such points so that I can have one piece of the kMean algorithm available

[<Test>]
    let ``list of centroids for separate lists of points``()=
        let listsOfPoints = [[{X=1.0;Y=0.0};{X=0.0;Y=0.0};{X= -1.0;Y=0.0}];[{X=0.0;Y= -3.0};{X=0.0;Y= -2.0};{X= 0.0;Y= -1.0}]];
        let expectedCentroidsAndVariances =  [({X=0.0;Y=0.0},2.0);({X=0.0;Y= -2.0},2.0)]

        let actualCentroidsAndVariances = centroidsAndVariancesForSeparateSegments listsOfPoints 

        Assert.AreEqual(2,List.length actualCentroidsAndVariances)
        Assert.AreEqual(expectedCentroidsAndVariances,actualCentroidsAndVariances)



[<Test>]
    let ``assign to partitions according to minimimum distance 0``()=
        let points = [{X= -3.0;Y=0.0};{X= -2.0;Y=0.0};{X= -1.0;Y=0.0}; {X= 1.0;Y=0.0};{X= 2.0;Y=0.0};{X= 3.0;Y=0.0}]
        let centroidGuesses = [{X = -2.0;Y=0.0}; {X = 2.0;Y = 0.0}]


        let expected = List.rev [{Point={X= -3.0;Y=0.0};AssignedCentroid={X= - 2.0;Y = 0.0}};
                                 {Point={X= -2.0;Y=0.0};AssignedCentroid={X= -2.0;Y= 0.0}};
                                 {Point={X= -1.0;Y=0.0};AssignedCentroid={X= -2.0;Y= 0.0}};
                                 {Point={X = 1.0;Y=0.0};AssignedCentroid={X=2.0;Y=0.0}};
                                 {Point={X= 2.0;Y=0.0};AssignedCentroid={X= 2.0;Y=0.0}};
                                 {Point={X= 3.0;Y=0.0};AssignedCentroid={X= 2.0;Y=0.0}}]
 
        let partitioned = tagPointsWithClosestCentroid centroidGuesses points
 
        Assert.AreEqual(expected,partitioned)
        
[<Test>]
    let ``assign to partitions according to minimimum distance 0 sdfasfsd``()=
        let points = [{X= -3.0;Y=0.0};{X= -2.0;Y=0.0};{X= -1.0;Y=0.0}; {X= 1.0;Y=0.0};{X= 2.0;Y=0.0};{X= 3.0;Y=0.0}]
        let centroidGuesses = [{X = -2.0;Y=0.0}; {X = 2.0;Y = 0.0}]

        let expected = List.rev [{Point={X= -3.0;Y=0.0};AssignedCentroid={X= - 2.0;Y = 0.0}};
          {Point={X= -2.0;Y=0.0};AssignedCentroid={X= -2.0;Y= 0.0}};{Point={X= -1.0;Y=0.0};AssignedCentroid={X= -2.0;Y= 0.0}};
          {Point={X = 1.0;Y=0.0};AssignedCentroid={X=2.0;Y=0.0}};{Point={X= 2.0;Y=0.0};AssignedCentroid={X= 2.0;Y=0.0}};{Point={X= 3.0;Y=0.0};AssignedCentroid={X= 2.0;Y=0.0}}]
 
        let partitioned = tagPointsWithClosestCentroid centroidGuesses points
 
        Assert.AreEqual(expected,partitioned)

[<Test>]
    let ``assign to partitions according to minimimum distance``()=
        let points = [{X= -2.0;Y=0.0};{X= 2.0;Y=0.0};{X=1.0;Y=0.0}]
        let centroidGuesses = [{X = -2.0;Y=0.0}; {X = 2.0;Y = 0.0}]

        let expected = [{Point={X=1.0;Y=0.0};AssignedCentroid={X=2.0;Y=0.0}};
                        {Point={X= 2.0;Y=0.0};AssignedCentroid={X= 2.0;Y= 0.0}};
                        {Point={X= -2.0;Y=0.0};AssignedCentroid={X= -2.0;Y=0.0}}]

        let partitioned = tagPointsWithClosestCentroid centroidGuesses points
 
        Assert.AreEqual(expected,partitioned)
 
[<Test>]
        let ``closest centroid is the first``()=
            let point = {X=0.0;Y=0.0}
            let centroids = [{X = 0.0;Y=1.0}; {X = 0.0;Y = 1.5}]

            let closestCentroid = closestCentroid point centroids

            Assert.AreEqual(List.head centroids, closestCentroid)

[<Test>]
        let ``closest centroid is the second``()=
            let point = {X=0.0;Y=0.0}
            let centroids = [{X = 1.0;Y=0.0}; {X = 0.5;Y = 0.0}]

            let closestCentroid = closestCentroid point centroids

            Assert.AreEqual(List.head (List.tail centroids), closestCentroid)

[<Test>]
        let ``closest centroid is in the middle``()=
            let point = {X=0.0;Y=0.0}
            let centroids = [{X = -1.0;Y=0.0}; {X = 0.3; Y= -0.3}; {X = 0.0;Y = 1.0}]

            let closestCentroid = closestCentroid point centroids

            Assert.AreEqual(List.head (List.tail centroids), closestCentroid)

[<Test>]
        let ``the centroid is the middle one``()=
            let points = [{X= -1.0;Y= -1.0};{X=0.0;Y=0.0};{X=1.0;Y=1.0}]
            let (centroid,_) = centroidAndVariance points
            Assert.AreEqual({X=0.0;Y=0.0},centroid)

[<Test>]
        let ``the centroid is the last one``()=
            let points = [{X= -1.0;Y= -1.0};{X=1.0;Y=1.0};{X=0.0;Y=0.0}]
            let (centroid,_) = centroidAndVariance points
            Assert.AreEqual({X=0.0;Y=0.0},centroid)

[<Test>]
        let ``the centroid is the first one``()=
            let points = [{X= 0.0;Y= 0.0};{X= -1.0;Y= -1.0};{X=1.0;Y=1.0}]
            let (centroid,_) = centroidAndVariance points
            Assert.AreEqual({X=0.0;Y=0.0},centroid)

[<Test>]
        let ``complete story of iterating for centroids``()=
            let points = [{X= -3.0;Y= 0.0};{X= -2.0;Y= 0.0};{X= -1.0;Y=0.0};{X= 1.0;Y= 0.0};{X= 2.0;Y= 0.0};{X= 3.0;Y=0.0}]
            let initialCentroids = [{X= -3.0;Y=0.0};{X=3.0;Y=0.0}]

            let actualListOfSeparateClusters = segmentPointsByClosestCentroid points initialCentroids
            let expected = List.rev [[{X= -3.0;Y= 0.0};{X= -2.0;Y= 0.0};{X= -1.0;Y=0.0}];[{X= 1.0;Y= 0.0};{X= 2.0;Y= 0.0};{X= 3.0;Y=0.0}]]

            Assert.AreEqual(2,List.length expected)
            Assert.AreEqual(Set.ofList (List.head expected), Set.ofList (List.head actualListOfSeparateClusters))
            Assert.AreEqual(Set.ofList (List.head (List.tail expected)), Set.ofList (List.head (List.tail actualListOfSeparateClusters)))

            let nextCentroidsAndV = centroidsAndVariancesForSeparateSegments expected

            let expectedNextCentroidsAndV = [({X=2.0;Y=0.0},2.0);({X= -2.0;Y=0.0},2.0)]

            Assert.AreEqual(expectedNextCentroidsAndV,nextCentroidsAndV)

            let nextCentroids = centroids expected

            let expectedNextCentroids = [{X=2.0;Y=0.0};{X= -2.0;Y=0.0}]

            Assert.AreEqual(expectedNextCentroids,nextCentroids)


[<Test>]
         let ``finding centroids iteratively``()=
            let points = [{X= -3.0;Y= 0.0};{X= -2.0;Y= 0.0};{X= -1.0;Y=0.0};{X= 1.0;Y= 0.0};{X= 2.0;Y= 0.0};{X= 3.0;Y=0.0}]
            let initialCentroids = [{X= -3.0;Y=0.0};{X= -2.0;Y=0.0}]
            let finalCentroids = iterateFindingFinalCentroids points initialCentroids
            printf "%f\n" (List.head finalCentroids).X
            printf "%f\n" (List.head finalCentroids).Y

            printf "%f\n" (List.head (List.tail finalCentroids)).X
            printf "%f\n" (List.head (List.tail finalCentroids)).Y

            Assert.AreEqual({X= 2.0;Y=0.0},List.head finalCentroids)
            Assert.AreEqual({X= -2.0;Y=0.0},List.head (List.tail finalCentroids))

[<Test>]
[<Ignore>]
        let ``finding centroids iteratively 2``()=
            let points = [{X= -3.0;Y= -3.0};{X= -2.0;Y= -2.0};{X= -1.0;Y= -1.0};{X= 1.0;Y= 1.0};{X= 2.0;Y= 2.0};{X= 3.0;Y=3.0}]
            let initialCentroids = [{X= -3.0;Y= -3.0};{X= -2.0;Y= -2.0}]
            let finalCentroids = iterateFindingFinalCentroids points initialCentroids
            printf "%f\n" (List.head finalCentroids).X
            printf "%f\n" (List.head finalCentroids).Y

            printf "%f\n" (List.head (List.tail finalCentroids)).X
            printf "%f\n" (List.head (List.tail finalCentroids)).Y

            Assert.AreEqual({X= 2.0;Y=2.0},List.head finalCentroids)
            Assert.AreEqual({X= -2.0;Y= -2.0},List.head (List.tail finalCentroids))

[<Test>]
        let ``finding centroids iteratively asdfasd``()=
            let normal1X = new Normal(1.0,4.0)
            let points = [{X= -3.0;Y= -3.0};{X= -2.0;Y= -2.0};{X= -1.0;Y= -1.0};{X= 1.0;Y= 1.0};{X= 2.0;Y= 2.0};{X= 3.0;Y=3.0}]
            let initialCentroids = [{X= -3.0;Y= -3.0};{X= -2.0;Y= -2.0}]
            let finalCentroids = iterateFindingFinalCentroids points initialCentroids
            printf "%f\n" (List.head finalCentroids).X
            printf "%f\n" (List.head finalCentroids).Y

            printf "%f\n" (List.head (List.tail finalCentroids)).X
            printf "%f\n" (List.head (List.tail finalCentroids)).Y

            Assert.AreEqual({X= 2.0;Y=2.0},List.head finalCentroids)
            Assert.AreEqual({X= -2.0;Y= -2.0},List.head (List.tail finalCentroids))

[<Test>]
        let ``find centroid for a single segment of uniform random data``()=
            let rnd = new Random();
            let uniform= new ContinuousUniform(0.0,1.0);
            let value = uniform.Sample();
            let values =  List.map (fun x -> {X=uniform.Sample();Y=uniform.Sample()})  [0 .. 999]
            let initialCentroids = [values.[rnd.Next() % 1000]]

            let finalCentroid = iterateFindingFinalCentroids values initialCentroids
            printf "%f\n" finalCentroid.[0].X
            printf "%f\n" finalCentroid.[0].Y

            let expectedCentroid = {X=uniform.Mean;Y=uniform.Mean}
            let tollerance = 0.05 // despite the tollerance given, there is still a very small probability that the test fails, when actual differ by more then tollerance

            Assert.AreEqual(finalCentroid.[0].X, expectedCentroid.X,0.05)
            Assert.AreEqual(finalCentroid.[0].Y, expectedCentroid.Y,0.05)

[<Test>]
        let ``find centroid for a single segment of uniform random data 2``()=
            let rnd = new Random();
            let uniformX= new ContinuousUniform(0.0,1.0);
            let uniformY = new ContinuousUniform(3.0,6.0);


            let values =  List.map (fun x -> {X=uniformX.Sample();Y=uniformY.Sample()})  [0 .. 999]
            let initialCentroids = [values.[rnd.Next() % 1000]]

            let finalCentroid = iterateFindingFinalCentroids values initialCentroids
            printf "%f\n" finalCentroid.[0].X
            printf "%f\n" finalCentroid.[0].Y


            let expectedCentroid = {X=uniformX.Mean;Y=uniformY.Mean}
            let tollerance = 0.05 // despite the tollerance given, there is still a very small probability that the test fails, when actual differ by more then tollerance

            Assert.AreEqual(finalCentroid.[0].X, expectedCentroid.X,0.05)
            Assert.AreEqual(finalCentroid.[0].Y, expectedCentroid.Y,0.05*3.0)


[<Test>]
        let ``find centroid for two single non overlapped segments of uniform random data, initial centroids are taken from their segments``()=
            let rnd = new Random();
            let uniformX1= new ContinuousUniform(0.0,1.0);
            let uniformY1 = new ContinuousUniform(0.0,1.0);

            let uniformX2= new ContinuousUniform(3.0,6.0);
            let uniformY2 = new ContinuousUniform(3.0,6.0);

            let firstSegment =  List.map (fun x -> {X=uniformX1.Sample();Y=uniformY1.Sample()})  [0 .. 999]
            let secondSegment =  List.map (fun x -> {X=uniformX2.Sample();Y=uniformY2.Sample()})  [0 .. 999]

            let initialCentroids = [firstSegment.[rnd.Next(1000)];secondSegment.[rnd.Next(1000)]]

            let finalCentroid = iterateFindingFinalCentroids (firstSegment@secondSegment) initialCentroids
            printf "%f\n" finalCentroid.[0].X
            printf "%f\n" finalCentroid.[0].Y

            printf "%f\n" finalCentroid.[1].X
            printf "%f\n" finalCentroid.[1].Y

            let expectedCentroid1 = {X=uniformX1.Mean;Y=uniformY1.Mean}
            let expectedCentroid2 = {X=uniformX2.Mean;Y=uniformY2.Mean}

            let tollerance = 0.05 // despite the tollerance given, there is still a very small probability that the test fails, when actual differ by more then tollerance

            // the first or the second centroid may converge to the mean of the first or the second mean 
            Assert.IsTrue(Math.Abs(finalCentroid.[0].X - expectedCentroid1.X) < tollerance || Math.Abs(finalCentroid.[1].X - expectedCentroid1.X) < tollerance)
            Assert.IsTrue(Math.Abs(finalCentroid.[0].Y - expectedCentroid1.Y) < tollerance*3.0 || Math.Abs(finalCentroid.[1].Y - expectedCentroid1.Y) < tollerance*3.0)


[<Test>]
[<Ignore>] // ignored only because it  is a little bit slow
        let ``find centroid for two single non overlapped segments of uniform random data, initial centroids are taken both from the first segment``()=
            let rnd = new Random();
            let uniformX1= new ContinuousUniform(0.0,1.0);
            let uniformY1 = new ContinuousUniform(0.0,1.0);

            let uniformX2= new ContinuousUniform(3.0,6.0);
            let uniformY2 = new ContinuousUniform(3.0,6.0);

            let firstSegment =  List.map (fun x -> {X=uniformX1.Sample();Y=uniformY1.Sample()})  [0 .. 999]
            let secondSegment =  List.map (fun x -> {X=uniformX2.Sample();Y=uniformY2.Sample()})  [0 .. 999]

            let initialCentroids = [firstSegment.[rnd.Next(1000)];firstSegment.[rnd.Next(1000)]]

            let finalCentroid = iterateFindingFinalCentroids (firstSegment@secondSegment) initialCentroids
            printf "%f\n" finalCentroid.[0].X
            printf "%f\n" finalCentroid.[0].Y

            printf "%f\n" finalCentroid.[1].X
            printf "%f\n" finalCentroid.[1].Y

            let expectedCentroid1 = {X=uniformX1.Mean;Y=uniformY1.Mean}
            let expectedCentroid2 = {X=uniformX2.Mean;Y=uniformY2.Mean}

            let tollerance = 0.05 // despite the tollerance given, there is still a very small probability that the test fails, when actual differ by more then tollerance

            Assert.IsTrue(Math.Abs(finalCentroid.[0].X - expectedCentroid1.X) < tollerance || Math.Abs(finalCentroid.[1].X - expectedCentroid1.X) < tollerance)
            Assert.IsTrue(Math.Abs(finalCentroid.[0].Y - expectedCentroid1.Y) < tollerance*3.0 || Math.Abs(finalCentroid.[1].Y - expectedCentroid1.Y) < tollerance*3.0)




[<Test>]
        let ``random data collected from four guassians, four centroids around the corner - final centroid will contain also the segment``()= 
            let rnd = System.Random()
            let generators = [|
              {XGauss=new Normal(1.01,2.0);YGauss= new Normal(0.99,2.0)};   
              {XGauss=new Normal(0.99,2.0);YGauss= new Normal(7.99,2.0)};
              {XGauss=new Normal(8.01,2.0);YGauss= new Normal(8.0,2.0)};
              {XGauss=new Normal(7.99,2.0);YGauss= new Normal(1.0,2.0)}
            |]

            let sampleFirsts = [
                {X=generators.[0].XGauss.Sample();Y=generators.[0].YGauss.Sample()};
                {X=generators.[1].XGauss.Sample();Y=generators.[1].YGauss.Sample()};
                {X=generators.[2].XGauss.Sample();Y=generators.[2].YGauss.Sample()};
                {X=generators.[3].XGauss.Sample();Y=generators.[3].YGauss.Sample()}
            ]
                
            let sample = sampleFirsts @ (List.map (fun x -> {X=x.XGauss.Sample();Y=x.YGauss.Sample()}) (List.map (fun x -> generators.[x % 4 ]) [0..995]))

            let initialCentroids = [sample.[0]; sample.[1]; sample.[2];sample.[3]]

            let finalCentroids = iterateFindingFinalCentroids sample initialCentroids

            // recover the segments and variance for each centroid

            let segment0 = List.fold (fun acc item -> if closestCentroid item finalCentroids = finalCentroids.[0] then item::acc else acc) [] sample
            let segment1 = List.fold (fun acc item -> if closestCentroid item finalCentroids = finalCentroids.[1] then item::acc else acc) [] sample
            let segment2 = List.fold (fun acc item -> if closestCentroid item finalCentroids = finalCentroids.[2] then item::acc else acc) [] sample
            let segment3 = List.fold (fun acc item -> if closestCentroid item finalCentroids = finalCentroids.[3] then item::acc else acc) [] sample

            let quadraticDist0 = sumOfQuadraticDistances finalCentroids.[0] segment0
            let quadraticDist1 = sumOfQuadraticDistances finalCentroids.[1] segment1
            let quadraticDist2 = sumOfQuadraticDistances finalCentroids.[2] segment2
            let quadraticDist3 = sumOfQuadraticDistances finalCentroids.[3] segment3


            printf "initial centroids \n" 
            printf "X = %f " initialCentroids.[0].X
            printf "Y = %f \n" initialCentroids.[0].Y
            printf "X = %f " initialCentroids.[1].X
            printf "Y = %f \n" initialCentroids.[1].Y
            printf "X = %f " initialCentroids.[2].X
            printf "Y = %f \n" initialCentroids.[2].Y
            printf "X = %f " initialCentroids.[3].X
            printf "Y = %f \n\n" initialCentroids.[3].Y

            printf "final centroids \n" 
            printf "X = %f " finalCentroids.[0].X
            printf "Y = %f " finalCentroids.[0].Y
            printf "Var = %f\n" quadraticDist0
            printf "X = %f " finalCentroids.[1].X
            printf "Y = %f " finalCentroids.[1].Y
            printf "Var = %f\n" quadraticDist1
            printf "X = %f " finalCentroids.[2].X
            printf "Y = %f " finalCentroids.[2].Y
            printf "Var = %f\n" quadraticDist2
            printf "X = %f " finalCentroids.[3].X
            printf "Y = %f " finalCentroids.[3].Y
            printf "Var = %f\n" quadraticDist3

            printf "Total Variance = %f \n" (quadraticDist0 + quadraticDist1 + quadraticDist2 + quadraticDist3)

            Assert.IsTrue(false) // a trick to make the test  show the standard output when it runs







                                  
