﻿module kMean.KMeans

open System
open MathNet.Numerics.Distributions

type Point = {X: float; Y: float}

//type Point = float list

type TaggedPoints = {Point: Point; AssignedCentroid: Point}
type CentroidAndSegment = {Centroid: Point; Segment: Point list}
type CentroidAndVarianceOfSegment = {Centroid: Point; Variance: double; Segment: Point list}

type BiGaussian = {XGauss:Normal;YGauss:Normal}

let quadraticDistance point1 point2 = 
    Math.Pow((point1.X-point2.X),2.0)+ Math.Pow((point1.Y-point2.Y),2.0)


// the total quadratic distance|dispertion of a point respect to a set of points
let sumOfQuadraticDistances point points = 
    List.sumBy (fun item -> (quadraticDistance point item)) points

         
// given a point and a set of centroids, returns the closest centroid 

let closestCentroid point centroids  =
    let precondition = match centroids with | [] -> failwith "Centroid list should be not empty" | _ -> true
    List.fold (fun acc item -> if ((quadraticDistance item point) < (quadraticDistance acc point)) then item else acc) (List.head centroids) centroids

let tagPointsWithClosestCentroid centroids segment =
    List.fold (fun acc item -> {Point=item; AssignedCentroid = (closestCentroid item centroids)}::acc) [] segment

// return the different lists of points by closest centroids      
let segmentPointsByClosestCentroid points centroids =
    let taggedPoints = tagPointsWithClosestCentroid centroids points
    List.fold (fun acc item -> (List.filter (fun p -> p.AssignedCentroid = item) taggedPoints |> List.map (fun taggedPoint -> taggedPoint.Point))::acc) [] centroids
                            
let centroidAndVariance segment = 
    let checkPrecondition = match segment with |[] -> failwith "empty segment is not valid" | _ -> true
    let Xs = List.sumBy (fun item -> item.X) segment
    let Ys = List.sumBy (fun  item -> item.Y) segment
    let centroid = {X=(Xs/(float (List.length segment)));Y=(Ys/(float (List.length segment)))}
    let variance = sumOfQuadraticDistances centroid segment
    (centroid,variance)

let centroid segment = 
    let checkPrecondition = match segment with |[] -> failwith "empty segment is not valid" | _ -> true
    let Xs = List.sumBy (fun item -> item.X) segment
    let Ys = List.sumBy (fun  item -> item.Y) segment
    let centroid = {X=(Xs/(float (List.length segment)));Y=(Ys/(float (List.length segment)))}
    centroid

let centroidsAndVariancesForSeparateSegments segments =
  List.map (fun x -> centroidAndVariance x) segments

let centroids segments =
  List.map (fun x -> centroid x) segments

let iterateFindingFinalCentroids points initialCentroids =
    let mutable currentCentroids = initialCentroids
    let mutable segments = segmentPointsByClosestCentroid points currentCentroids
    let mutable nextCentroids = centroids segments 
    let mutable nextSegments = segmentPointsByClosestCentroid points nextCentroids
    while ((List.sort nextSegments) <> (List.sort segments)) do
        currentCentroids <- nextCentroids
        segments <- nextSegments
        nextCentroids <- centroids nextSegments
        nextSegments <- segmentPointsByClosestCentroid points nextCentroids
    nextCentroids





