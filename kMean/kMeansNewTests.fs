﻿
module kMean.NewTests

open NUnit.Framework
open kMean.KMeansNew
open System
open MathNet.Numerics.Distributions


[<Test>]
[<Ignore>]
    let ``distance between two points``()=
        let point1 = {X=0.0;Y=0.0}
        let point2 = {X=1.0;Y=0.0}

        let expectedDistance = Math.Abs(1)
        let distance = quadraticDistance point1 point2
        Assert.AreEqual(1,distance)




[<Test>]
[<Ignore>]
    let ``total distance of a point respect to a given set of points``()=
        let point = {X=0.0;Y=0.0}
        let points = [{X=2.0;Y=0.0};{X=0.0;Y=2.0}]

        let expectedDistance = 8.0 // 2^2 + 2^2

        Assert.AreEqual(expectedDistance,sumOfQuadraticDistances point points)

[<Test>]
[<Ignore>]
    let ``total distance between a point and the other points``()=
        let firstPoint = {X=0.0;Y=0.0}
        let secondPoint = {X=0.0;Y=1.0}
        let points = [firstPoint; secondPoint]
        let actualDistance = sumOfQuadraticDistances firstPoint points
        let expectedDistance = 1

        Assert.AreEqual(expectedDistance,actualDistance)


[<Test>]
[<Ignore>]
    let ``total distance between a point and the other points 2``()=
        let firstPoint = {X=0.0;Y=0.0}
        let secondPoint = {X=0.0;Y=1.0}
        let thirdPoint = {X=0.0;Y=3.0}

        let points = [firstPoint; secondPoint;thirdPoint]
        let actualDistance = sumOfQuadraticDistances firstPoint points 
        let expectedDistance = 10 // 1^2 + 3^2

        Assert.AreEqual(expectedDistance,actualDistance)

[<Test>]
        let ``centroid test``()=
            let firstPoint = {X=0.0;Y=0.0}
            let secondPoint = {X=0.0;Y=1.0}
            let thirdPoint = {X=0.0;Y=2.0}

            let points = [firstPoint; secondPoint; thirdPoint]

            let (centroid,variance) = centroidAndVariance points
            Assert.AreEqual(secondPoint,centroid)
            Assert.AreEqual(2.0,variance)

[<Test>]
    let ``partitions and find centroids of a one step iteration``()=
        let points = [{X= -3.0;Y=0.0};{X= -2.0;Y=0.0};{X= -1.0;Y=0.0};{X=1.0;Y=0.0};{X=2.0;Y=0.0};{X=3.0;Y=0.0}]
        let centroidGuesses = [{X = -3.0;Y=0.0}; {X = 3.0;Y = 0.0}]

        let expected = List.rev  [
                        {Point={X= -3.0;Y=0.0};AssignedCentroid={X= -3.0;Y=0.0}};
                        {Point={X= -2.0;Y=0.0};AssignedCentroid={X= -3.0;Y=0.0}};
                        {Point={X= -1.0;Y=0.0};AssignedCentroid={X= -3.0;Y=0.0}};
                        {Point={X=  1.0;Y=0.0};AssignedCentroid={X=  3.0;Y=0.0}};
                        {Point={X=  2.0;Y=0.0};AssignedCentroid={X=  3.0;Y=0.0}};
                        {Point={X=  3.0;Y=0.0};AssignedCentroid={X=  3.0;Y=0.0}}
                     ]

        let partitioned = tagPointsWithClosestCentroid centroidGuesses points

        Assert.AreEqual(expected,partitioned)

        let expectedCentroids = [{X= 2.0; Y=0.0}; {X = -2.0;Y=0.0}]

        let actual = iterateFindingFinalCentroids points centroidGuesses

        Assert.AreEqual(expectedCentroids,actual)


[<Test>]
    let ``assign to partitions according to minimimum distance``()=
        let points = [{X= -2.0;Y=0.0};{X= 2.0;Y=0.0};{X=1.0;Y=0.0}]
        let centroidGuesses = [{X = -2.0;Y=0.0}; {X = 2.0;Y = 0.0}]

        let expected = [{Point={X=1.0;Y=0.0};AssignedCentroid={X=2.0;Y=0.0}};
                        {Point={X= 2.0;Y=0.0};AssignedCentroid={X= 2.0;Y= 0.0}};
                        {Point={X= -2.0;Y=0.0};AssignedCentroid={X= -2.0;Y=0.0}}]

        let partitioned = tagPointsWithClosestCentroid centroidGuesses points
 
        Assert.AreEqual(expected,partitioned)
 


[<Test>]
    let ``assign to partitions according to minimimum distance 0 sdfasfsd``()=
        let points = [{X= -3.0;Y=0.0};{X= -2.0;Y=0.0};{X= -1.0;Y=0.0}; {X= 1.0;Y=0.0};{X= 2.0;Y=0.0};{X= 3.0;Y=0.0}]
        let centroidGuesses = [{X = -2.0;Y=0.0}; {X = 2.0;Y = 0.0}]

        let expected = List.rev [{Point={X= -3.0;Y=0.0};AssignedCentroid={X= - 2.0;Y = 0.0}};
          {Point={X= -2.0;Y=0.0};AssignedCentroid={X= -2.0;Y= 0.0}};{Point={X= -1.0;Y=0.0};AssignedCentroid={X= -2.0;Y= 0.0}};
          {Point={X = 1.0;Y=0.0};AssignedCentroid={X=2.0;Y=0.0}};{Point={X= 2.0;Y=0.0};AssignedCentroid={X= 2.0;Y=0.0}};{Point={X= 3.0;Y=0.0};AssignedCentroid={X= 2.0;Y=0.0}}]
 
        let partitioned = tagPointsWithClosestCentroid centroidGuesses points
 
        Assert.AreEqual(expected,partitioned)


[<Test>] // splike
    let ``totalDistanceThreePoints``()=
        let firstPoint = {X=0.0;Y=0.0}
        let secondPoint = {X=0.0;Y=1.0}
        let thirdPoint = {X=0.0;Y=2.0}

        let points = [firstPoint; secondPoint;thirdPoint]
        let distance1 = sumOfQuadraticDistances firstPoint points
        let distance2 = sumOfQuadraticDistances secondPoint points
        let distance3 = sumOfQuadraticDistances thirdPoint points

        Assert.AreEqual(5,distance1)
        Assert.AreEqual(2,distance2)
        Assert.AreEqual(5,distance3)

        let actualCentroidsMap = List.map (fun x -> (x, sumOfQuadraticDistances x points)) points

        let centroid = centroidAndVariance points


        Assert.AreEqual(({X=0.0;Y=0.0},5.0),actualCentroidsMap.[0])
        Assert.AreEqual(({X=0.0;Y=1.0},2.0),actualCentroidsMap.[1]) // this is the centroid
        Assert.AreEqual(({X=0.0;Y=2.0},5.0),actualCentroidsMap.[2])

        Assert.AreEqual(({X=0.0;Y=1.0},2.0),centroid)




[<Test>]
        let ``I need the list of the centroids, their variance, and their segment``()= 
            let rnd = System.Random()
            let generators = [|
              {XGauss=new Normal(1.01,4.0);YGauss= new Normal(0.99,4.0)};
              {XGauss=new Normal(0.99,4.0);YGauss= new Normal(7.99,4.0)};
              {XGauss=new Normal(8.01,4.0);YGauss= new Normal(8.0,4.0)};
              {XGauss=new Normal(7.99,4.0);YGauss= new Normal(1.0,4.0)}
            |]
            let sample = List.map (fun x -> {X=x.XGauss.Sample();Y=x.YGauss.Sample()}) (List.map (fun x -> generators.[rnd.Next(4) ]) [0..1000])

//            let sample = List.map (fun x -> {X=x.XGauss.Sample();Y=x.YGauss.Sample()}) (List.map (fun x -> generators.[x % 4 ]) [0..1000])
//            sample.[0] <- {X=generators.[0].XGauss.Sample();Y=generators.[0].YGauss.Sample()}
//            let initialCentroids = [sample.[rnd.Next(0,999)]; sample.[rnd.Next(0,999)]; sample.[rnd.Next(0,999)];sample.[rnd.Next(0,999)]]

            let initialCentroids = [sample.[0]; sample.[1]; sample.[2];sample.[3]]


            let finalCentroids = iterateFindingFinalCentroidsXX sample initialCentroids

            let firstFinalCentroids = List.head finalCentroids
            let check = match firstFinalCentroids with 
                | {Centroid= _; Variance= _; Segment=  _} -> "ok"
                | _ -> failwith "problem with centroid and stuff"
                
//
//            printf "%f\n" finalCentroids.[0].X
//            printf "%f\n" finalCentroids.[0].Y
//            printf "%f\n" finalCentroids.[1].X
//            printf "%f\n" finalCentroids.[1].Y
//            printf "%f\n" finalCentroids.[2].X
//            printf "%f\n" finalCentroids.[2].Y
//            printf "%f\n" finalCentroids.[3].X
//            printf "%f\n" finalCentroids.[3].Y

            Assert.IsTrue(false)


